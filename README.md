# Docker image for Oxpt-dev

## Requirements
- Docker
- Docker Compose

## Usage
### First time
1. Clone repo.
2. `$ ./init.sh`
3. Access [localhost:4000](localhost:4000) in your browser.

### After the second times
1. `$ docker-compose up`
  (without logs: `$ docker-compose up -d`)

## Install external games
1. Open install.sh in your editor and Modify modules.
2. `$ ./install.sh`
3. `$ docker-compose up`

