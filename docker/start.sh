#!/usr/bin/env bash
echo "[mix deps.get]"
mix deps.get

echo ""
echo ""
echo "[mix ecto.create]"
mix ecto.create

echo ""
echo ""
echo "[mix ecto.migrate]"
mix ecto.migrate

echo ""
echo ""
echo "[npm install]"
cd apps/oxpt_web/assets
npm install
cd ../..

for app in ./oxpt_*; do
  if [ $app != "oxpt_web" ]; then
    cd $app
    echo "[${app}]"
    npm install
    cd ..
  fi
done

echo ""
echo ""
echo "[mix phx.server]"
cd ..
mix phx.server
