#!/usr/bin/env bash

echo "[Clone Repo]"
git clone https://gitlab.com/xeejp/oxpt_umbrella.git oxpt

echo "[Docker compose build]"
docker-compose build

echo "[Docker compose up]"
docker-compose up
#docker-compose up -d
