#!/usr/bin/env bash

host="https://gitlab.com/xeejp/"
#host="git@gitlab.com:xeejp/"

modules=(
    "oxpt_questionnaire"
    "oxpt_questionnaire_host"
)

cd oxpt/apps
for module in "${modules[@]}" ; do
    git clone $host$module".git"
    echo ""
    echo "Add following line to OxptWeb.MixProject::deps/0 in apps/oxpt_web/mix.exs"
    echo \{:$module, in_umbrella: true\},
    echo ""
    echo "Add following line to OxptWeb.EndPoint::reloadable_apps in apps/oxpt_web/config/config.exs"
    echo :$module,
    echo ""
    echo "Add following line into \':oxpt_web, :games, []' in apps/oxpt_web/config/config.exs"
    echo \{:$module, \{Oxpt.$module, \[\]\}\},
    echo ""
done

